const verificarGet = require("../middleware/verificarGet");

const verificarToken = require("../middleware/verificarToken");

module.exports = function (app) {
  app.get("/productos", async function (req, res) {
    const productos = require("../services/productosServices");

    const response = await productos.getProductos();

    res.send(response.result);
  });

  app.get(
    "/productos/:id",
    verificarGet.verificarNumero,
    async function (req, res) {
      const id = req.params.id;

      const productos = require("../services/productosServices");

      const response = await productos.getByIdProductos(id);

      res.send(response.result);
    }
  );

  app.post(
    "/productos",
    verificarToken.verificar,
    verificarToken.admin,
    async function (req, res) {
      const productosNuevo = req.body;

      const productos = require("../services/productosServices");
      const response = await productos.postProductos(productosNuevo);
      if (response.error) {
        res.send(response.error);
      } else {
        res.send(response.result);
      }
    }
  );
  app.put("/productos", async function (req, res) {
    const productosModifica = req.body;

    const productos = require("../services/productosServices");
    const response = await productos.putProductos(productosModifica);
    if (response.error) {
      res.send(response.error);
    } else {
      res.send(response.result);
    }
  });
  app.delete(
    "/productos/:id",
    verificarGet.verificarNumero,
    async function (req, res) {
      const id = req.params.id;

      const productos = require("../services/productosServices");

      const response = await productos.deleteProductos(id);

      res.send(response.result);
    }
  );
};
