//Agrego Middleware
const verificarToken = require("../middleware/verificarToken");
//Agrego Middleware
const verificarGet = require("../middleware/verificarGet");
//creamos el módulo a exportar
//Al ser llamado en index.js recibe las capacidades de express, para ser utilizado
module.exports = function (app) {
  app.post("/ventas", verificarToken.verificar, async function (req, res) {
    const ventaNueva = req.body;
    const ventas = require("./../services/ventasServices");
    const response = await ventas.postVentas(ventaNueva);
    if (response.error) {
      res.send(response.error);
    } else {
      res.send(response.result);
    }
  });
  // Creamos la ruta obtener todas las ventas.
  app.get("/ventas", async function (req, res) {
    //requerimos y guardamos la ruta de services donde hara la consulta a la base
    const ventas = require("./../services/ventasServices");
    //hacemos la consulta y ponemos await porque no sabemos cuanto puede demorar
    //podemos seguir viendo el proceso en  ventaservices.
    const response = await ventas.getVentas();
    //Una vez recibida la respuesta, se la mandamos a la ruta
    res.send(response.result);
  });

  app.get(
    "/ventas/:id",
    verificarGet.verificarNumero,
    async function (req, res) {
      // tomamos el parametro de la ruta
      const id = req.params.id;
      const venta = require("./../services/ventasServices");
      const response = await venta.getVentasById(id);
      if (response.error) {
        res.send(response.error);
      } else {
        res.send(response.result);
      }
    }
  );
};
