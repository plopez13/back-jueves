const verificarGet = require("../middleware/verificarGet");

const verificarToken = require("../middleware/verificarToken");

module.exports = function (app) {
  app.get("/rol", async function (req, res) {
    const rol = require("../services/rolServices");

    const response = await rol.getRol();

    res.send(response.result);
  });

  app.get("/rol/:id", verificarGet.verificarNumero, async function (req, res) {
    const id = req.params.id;

    const rol = require("../services/rolServices");

    const response = await rol.getByIdRol(id);

    res.send(response.result);
  });

  app.post(
    "/rol",
    verificarToken.verificar,
    verificarToken.admin,
    async function (req, res) {
      const rolNueva = req.body;

      const rol = require("../services/rolServices");
      const response = await rol.postRol(rolNueva);
      if (response.error) {
        res.send(response.error);
      } else {
        res.send(response.result);
      }
    }
  );

  app.put("/rol", async function (req, res) {
    const rolModificada = req.body;

    const rol = require("../services/rolServices");
    const response = await rol.putRol(rolModificada);
    if (response.error) {
      res.send(response.error);
    } else {
      res.send(response.result);
    }
  });

  app.delete(
    "/rol/:id",
    verificarGet.verificarNumero,
    async function (req, res) {
      const id = req.params.id;

      const rol = require("../services/rolServices");

      const response = await rol.deleteRol(id);

      res.send(response.result);
    }
  );
};
