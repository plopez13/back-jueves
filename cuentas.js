const cuentas = {
  sumar: (uno, dos) => {
    return uno + dos;
  },
  restar: (uno, dos) => {
    return uno - dos;
  },
};

module.exports = cuentas;
