//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada
const rol = {
  //dentro de ella ponemos una funcion asincrona, porque no sabemos cuanto demora la base en responder
  async getRol() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM rol WHERE `activo` = '0'";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdRol(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM rol WHERE id =" + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postRol(dato) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      'INSERT INTO `rol`(`id`, `nombre`) VALUES (NULL,"' + dato.nombre + '")';
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async putRol(dato) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `rol` SET `nombre`='" +
      dato.nombre +
      "' WHERE `id`= '" +
      dato.id +
      "'";
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "se modifico la categoria" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async deleteRol(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "UPDATE `rol` SET `activo`='1' WHERE `id`= '" + id + "'";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};
//Exportamos el módulo
module.exports = rol;
