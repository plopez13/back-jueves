//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

const entrada_mercaderia = {
  async getEntrada_mercaderia() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM entrada_mercaderia  WHERE `costo_activo` = '0'";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdEntrada_mercaderia(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM entrada_mercaderia WHERE id =" + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async postEntrada_mercaderia(ingreso) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      'INSERT INTO `entrada_mercaderia`(`id`, `producto_id`, `cantidad`, `costo`) VALUES (NULL,"' +
      ingreso.producto_id +
      '","' +
      ingreso.cantidad +
      '","' +
      ingreso.costo +
      '")';
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putEntrada_mercaderia(modifica) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `entrada_mercaderia` SET `producto_id`='" +
      modifica.producto_id +
      "', `cantidad`=" +
      modifica.cantidad +
      ", `costo`=" +
      modifica.costo +
      " WHERE `id`=" +
      modifica.id;
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "se modifico el producto" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async deleteEntrada_mercaderia(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `entada_mercaderia` SET `costo_activo` = 1 WHERE `costo` < 3";
    //Ponemos un await porque desconocemos la demora de la misma

    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};

module.exports = entrada_mercaderia;
