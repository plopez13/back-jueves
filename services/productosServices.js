//requerimos el módulo para conectarse a la base de datos
const mysql = require("mysql");
//requerimos el archivo donde tenemos configurada la conexion
const conn = require("../config/conn");

//creamos la constante a ser exportada
const productos = {
  async getProductos() {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM productos  WHERE `visible_venta` = '0'";
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },

  async getByIdProductos(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql = "SELECT * FROM rol WHERE id =" + id;
    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async postProductos(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      'INSERT INTO `productos`(`id`, `nombre`, `precio`, `stock`, `categoria_id`) VALUES (NULL,"' +
      nuevo.nombre +
      '","' +
      nuevo.precio +
      '","' +
      nuevo.stock +
      '","' +
      nuevo.categoria_id +
      '")';
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async putProductos(nuevo) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `productos` SET `nombre`='" +
      nuevo.nombre +
      "', `precio`=" +
      nuevo.precio +
      ", `stock`=" +
      nuevo.stock +
      ", `categoria_id`=" +
      nuevo.categoria_id +
      " WHERE `id`=" +
      nuevo.id;
    console.log(sql);

    //Con el archivo de conexion a la base, enviamos la consulta a la misma
    //Ponemos un await porque desconocemos la demora de la misma
    let resultado = await conn.query(sql);
    let response = { error: "se modifico el producto" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL de put" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
  async deleteProductos(id) {
    //Guardamos en una variable la consulta que queremos generar
    let sql =
      "UPDATE `productos` SET `visible_venta` = 1 WHERE `id` AND stock < 5";
    //Ponemos un await porque desconocemos la demora de la misma

    let resultado = await conn.query(sql);
    let response = { error: "No se encontraron registros" };
    if (resultado.code) {
      response = { error: "Error en la consulta SQL" };
    } else if (resultado.length > 0) {
      response = { result: resultado };
    }
    return response;
  },
};
module.exports = productos;
