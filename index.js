const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("dotenv").config();

const secret = process.env.SECRET;

require("./controllers/categoriasController")(app);

require("./controllers/loginController")(app);

require("./controllers/rolController")(app);

require("./controllers/ventasController")(app);

require("./controllers/productosController")(app);

require("./controllers/entrada_mercaderiaController")(app);

require("./controllers/clientesController")(app);

app.listen(3001, () => {
  console.log("servidor en puerto 3000");
});
